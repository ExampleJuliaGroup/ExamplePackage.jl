# ExamplePackage

[![Build Status](https://gitlab.com/ExampleJuliaGroup/ExamplePackage.jl/badges/master/build.svg)](https://gitlab.com/ExampleJuliaGroup/ExamplePackage.jl/pipelines)
[![Coverage](https://gitlab.com/ExampleJuliaGroup/ExamplePackage.jl/badges/master/coverage.svg)](https://gitlab.com/ExampleJuliaGroup/ExamplePackage.jl/commits/master)
[![Build Status](https://travis-ci.com/ExampleJuliaGroup/ExamplePackage.jl.svg?branch=master)](https://travis-ci.com/ExampleJuliaGroup/ExamplePackage.jl)
[![Coverage](https://codecov.io/gh/ExampleJuliaGroup/ExamplePackage.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/ExampleJuliaGroup/ExamplePackage.jl)
[![Coverage](https://coveralls.io/repos/github/ExampleJuliaGroup/ExamplePackage.jl/badge.svg?branch=master)](https://coveralls.io/github/ExampleJuliaGroup/ExamplePackage.jl?branch=master)
